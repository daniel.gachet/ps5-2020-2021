---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Cible pour les cours d'Architecture des Ordinateurs
filières:
  - Télécommunications 
  - Informatique
  - ISC
nombre d'étudiants: 1
mandants:
  - ISC
professeurs co-superviseurs:
  - Jacques Supcik
mots-clé: [microprocesseur, bare metal, programmation]
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.35\textwidth,height=\textheight]{img/up.jpg}
\end{center}
```

## Description/Contexte

Avec l'introduction de la nouvelle filière ISC (Informatique et Systèmes de Communication), 
un cours d'Architecture des Ordinateurs et un cours de Système d'Exploitation remplaceront 
les cours actuels de Systèmes Embarqués, de Systèmes d'Exploitation et de Programmation Concurrente.

Pour les travaux pratiques du cours d'Architecture des Ordinateurs, nous souhaitons utiliser
une cible plus légère que l'actuelle BeagleBone Black (p.ex. [STM32 Discovery kit IoT Node](https://www.st.com/en/evaluation-tools/b-l475e-iot01a.html)). 
Cette nouvelle cible devra néanmoins offrir des fonctionnalités suffisantes pour réaliser 
des travaux pratiques similaires à ceux pratiqués actuellement en programmation "bare metal".

L'objectif du projet est de choisir une nouvelle cible avec un environnement de développement 
(hardware et software) appropriée aux besoins du cours d'Architecture des Ordinateurs.

## Objectifs/Tâches

Les tâches principales sont les suivantes:

- Elaborer un cahier des charges en collaboration avec les responsables du projet
- Etudier le marché afin de trouver une cible adaptée aux besoins du cours
- Etudier les environnements de développements pour des réalisations en "bare metal"
- Mettre en oeuvre l'infrastructure (cible, périphériques, environnement de développement)
- Migrer des TPs choisis sur la nouvelle infrastructure
- Tester et valider l'infrastructure
- Rédiger une documentation adéquate

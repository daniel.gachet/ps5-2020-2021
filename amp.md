---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Real-Time Multiprocessing with Asymmetrical Multi-Core Processors
filières:
  - Télécommunications 
  - Informatique
  - ISC
nombre d'étudiants: 1
mandants:
  - iSIS
professeurs co-superviseurs:
  - Nicolas Schroeter
mots-clé: [Système embarqué, multi-coeurs asymetriques, Linux, RTOS, programmation]
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.5\textwidth,height=\textheight]{img/amp.png}
\end{center}
```

## Description/Contexte

Les fabricants de µPs proposent de plus en plus sur leurs processeurs multi-coeurs à très hautes performances des coeurs à basse consommation afin de réaliser et de garantir les parties aspects temps réel d'applications complexes tournant sous Linux. La compagnie [NXP](https://nxp.com) propose dans sa série de processeurs d'applications [i.MX8](https://www.nxp.com/products/processors-and-microcontrollers/arm-processors/i-mx-applications-processors/i-mx-8-processors:IMX8-SERIES) de tels µPs.

Dans le cadre d'un projet de Bachelor avec la maison [Asyril](https://www.asyril.com/fr/) à Villaz-St-Pierre, un [SoM MSC SM2S-IMX8M](https://www.avnet.com/wps/portal/integrated/products/embedded-boards/smarc-modules/MSC%20SM2S-IMX8M/) équipé d'un µP i.MX8M (4 cores Cortex-A53 et 1 core Cortex-M4F) a été choisi pour piloter les actionneurs du feeder de leur système d’alimentation de petits composants. L'application réalisée dans le cadre de ce projet a mis en oeuvre les contrôleurs I2S et GPIO du µP pour la génération de signaux sinusoïdaux permettant d'actionner des vibreurs du feeder. Cette application a été développée sous Linux et ne tourne que sur les µPs Cortex-A53.

L’objectif de ce projet est de migrer sur le coeur Cortex-M4 la partie de l’application qui gère les contrôleurs I2S et GPIO et de développer l’interface  de communication entre l’application Linux et le logiciel sur le cortex-M4.

## Objectifs/Tâches

Les tâches principales sont les suivantes:

- Elaborer un cahier des charges en collaboration avec les responsables du projet
- Analyser le système et proposer des choix pour la réalisation de l'application sur le µP Cortex-M4 (RTOS, messagerie, ...)
- Mettre en oeuvre le système choisi
- Concevoir et réaliser un prototype fonctionnel
- Tester et valider le système
- Rédiger une documentation adéquate

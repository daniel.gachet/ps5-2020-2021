---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: Snow detector
filières:
  - Informatique
nombre d'étudiants: 1
mandants:
  - MCA - Boschung 
professeurs co-superviseurs:
  - Serge Ayer
attribué à: 
  - Balleyguier Adrien Dino
mots-clé: [microcontrôlleur, programmation]
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.5\textwidth,height=\textheight]{img/snow.jpeg}
\end{center}
```

## Description/Contexte

Conception d’un capteur pouvant mesurer la hauteur de neige (création d’un nouveau capteur, basé sur d’autres principes de mesure et de communication).

- Mécanique : fabrication de la tête de mesure, conception du corps du capteur, mécanisme permettant à la tête de mesure de se mouvoir sur l’axe du capteur (mesure et position horizontales)
- Électronique : développement de l’électronique de commande, commande des moteurs pas à pas, alimentation de la tête de mesure avec des super caps
- Informatique : programmation des algorithmes de mesure (tête) et du microcontrôleur, communication sans fil depuis la tête de mesure vers l’électronique (Bluetooth par exemple)

## Objectifs/Tâches

Les tâches principales sont les suivantes:

- Elaborer un cahier des charges en collaboration avec les responsables du projet et les autres membres du projet
- Etudier l'état de l'art
- Concevoir et réaliser une maquette fonctionnelle en collaboration avec les autres membres du projet
- Tester et valider la maquette
- Rédiger une documentation adéquate
